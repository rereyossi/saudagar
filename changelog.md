= 1.16.9 (27 Februari 2021) =
* Fix: atur tinggi video iframe
* Fix: atur tinggi gambar
* Fix: setting grid kolom blog dan shop
* Fix: ajax load more pada elementor produk

= 1.16.1 (24 Februari 2021) =
* Fix: menampilkan tombol add to cart pada halaman produk

= 1.16.0 (24 Februari 2021) =

* Update: optimasi add to cart ajax
* Fix: customizer setting

= 1.15.4 (30 January 2021) =

* Update: fix mailchimp optin on checkout page
* Update: move sticky product button from plugin Webforia Shop Booster to Core Theme
* Update: move bottom shop navbar from plugin Webforia Shop Booster to Core Theme 

= 1.15.3 =
* Fix: fix elementor widget manual query

= 1.15.1 =
* Update: change add to cart ajax-admin.php to wc-ajax for perfome

= 1.15.0 =
* Update: postcode field on checkout and account change form required to opsional

= 1.14.0 =
* Update: remove unused checkout field 

= 1.13.1 = 
* Fix: fix height iframe

= 1.12.0 = 
* Update: Support WordPress 5.6
* Update: remove deprecated jquery function

= 1.10.4 =
* Fix: variation select title

= 1.8.16 =
* Update: add new tag body open
* Fix:change title and price on variation
* Fix: mini cart panel

= 1.7.10 =
* Fix: countdown timer archive product on same page
 
= 1.4.10 =
* Fix: thank page container width on mobile
* Fix: update theme template support woocommerce 3.9.0

= 1.4.2 =
* Fix: user login redirect to my account 

= 1.4.1 =
* Fix: redirect pop up login
* Update: New text domain
* Update: New control for hover colors
* Update: Added global colors

= 1.2.12 =
* Fix: blog thumbnail size

= 1.2.10 =
* Fix: label style on checkout page
* Fix: qty if stock not ready

= 1.2.6 =
* Fix: header builder update js

= 1.2.5 =
* Update: added setup wizard by merlinwp

= 1.2.0 =
* Update: added demo import
* Update: added header builder
* Update: remove code genator classes

= 1.0.0 =
* First release

== Resources ==

Saudagar bundles the following third-party resources:

* [Kirki](http://aristath.github.io/kirki/)
* [Font Awesome Free](https://fontawesome.com)
* [Owl Carousel](https://github.com/OwlCarousel2/OwlCarousel2)
* [Merlin Wp](https://github.com/richtabor/MerlinWP)