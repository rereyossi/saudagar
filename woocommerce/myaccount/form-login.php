<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 4.1.0
 */


/**
 * Retheme 1.0.0 - Overridden
 * To do:
 * - overridden all
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

do_action('woocommerce_before_customer_login_form');?>

<?php $form_class = apply_filters('rt_form_class', '');?>

		<div class="flex flex-center flex-row">
			<div class="flex-md-5 flex-sm-12">
				
				<div class="rt-form-login">

					<h2><?php esc_html_e('Login', 'woocommerce');?></h2>

					<form class="woocommerce-form woocommerce-form-login login mb-10" method="post">

						<?php do_action('woocommerce_login_form_start');?>

						<p class="rt-form <?php echo $form_class ?>">
							<label class="rt-form__label" for="username"><?php esc_html_e('Username or email address', 'woocommerce');?>&nbsp;<span class="required">*</span></label>
							<input type="text" class="rt-form__input" name="username" id="username" autocomplete="username" value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
						</p>
						<p class="rt-form <?php echo $form_class ?>
">
							<label class="rt-form__label" for="password"><?php esc_html_e('Password', 'woocommerce');?>&nbsp;<span class="required">*</span></label>
							<input class="rt-form__input" type="password" name="password" id="password" autocomplete="current-password" />
						</p>

						<?php do_action('woocommerce_login_form');?>

						<p class="flex flex-middle flex-between">
							<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
								<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e('Remember me', 'woocommerce');?></span>
							</label>
							<a class="lost_password link-text" href="<?php echo esc_url(wp_lostpassword_url()); ?>"><i class="fa fa-life-ring"></i> <?php esc_html_e('Lost your password?', 'woocommerce');?></a>
						</p>

						<div class="rt-form__group">
							<?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce');?>
							<button type="submit" class="rt-btn rt-btn--primary rt-btn--block" name="login" value="<?php esc_attr_e('Login', 'woocommerce');?>"><?php esc_html_e('Login', 'woocommerce');?></button>
						</div>

						<?php do_action('woocommerce_login_form_end');?>

					</form>

					<?php if (get_option('woocommerce_enable_myaccount_registration') === 'yes'): ?>
						<p class="rt-account-link-login text-center"><?php _e('Not a member?', RT_THEME_DOMAIN) ?> <a href="#" class="js-account-register"><?php _e('Join now.', RT_THEME_DOMAIN) ?></a></p>
					<?php endif ?>
				</div>

				<?php if (get_option('woocommerce_enable_myaccount_registration') === 'yes'): ?>

					<div class="rt-form-register" style="display: none;">

						<h2><?php esc_html_e('Register', 'woocommerce');?></h2>

						<form method="post" class="woocommerce-form woocommerce-form-register register mb-10" <?php do_action('woocommerce_register_form_tag');?> >

							<?php do_action('woocommerce_register_form_start');?>

							<?php if ('no' === get_option('woocommerce_registration_generate_username')): ?>

								<p class="rt-form <?php echo $form_class ?>
">
									<label for="reg_username"><?php esc_html_e('Username', 'woocommerce');?>&nbsp;<span class="required">*</span></label>
									<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo (!empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
								</p>

							<?php endif;?>

							<p class="rt-form <?php echo $form_class?>">
								<label class="rt-form__label" for="reg_email"><?php esc_html_e('Email address', 'woocommerce');?>&nbsp;<span class="required">*</span></label>
								<input type="email" class="rt-form__input" name="email" id="reg_email" autocomplete="email" value="<?php echo (!empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
							</p>

							<?php if ('no' === get_option('woocommerce_registration_generate_password')): ?>

								<p class="rt-form <?php echo $form_class?>">
									<label class="rt-form__label" for="reg_password"><?php esc_html_e('Password', 'woocommerce');?>&nbsp;<span class="required">*</span></label>
									<input type="password" class="rt-form__input" name="password" id="reg_password" autocomplete="new-password" />
								</p>

							<?php endif;?>

							<?php do_action('woocommerce_register_form');?>

							<p class="woocommerce-FormRow form-row">
								<?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce');?>
								<button type="submit" class="rt-btn rt-btn--block rt-btn--primary" name="register" value="<?php esc_attr_e('Register', 'woocommerce');?>"><?php esc_html_e('Register', 'woocommerce');?></button>
							</p>

							<?php do_action('woocommerce_register_form_end');?>

						</form>

						<p class="rt-account-link-login text-center"><?php _e('Already a member?', RT_THEME_DOMAIN) ?> <a href="#" class="js-account-login"><?php _e('Sign in.', RT_THEME_DOMAIN) ?></a></p>


					</div>

				<?php endif;?>
				
			</div>
		</div>

<?php do_action('woocommerce_after_customer_login_form');?>
