<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 5.2.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}?>
<div class="rt-mini-cart">
		

<?php do_action( 'woocommerce_before_mini_cart' ); ?>
	<?php if ( ! WC()->cart->is_empty() ) : ?>

		<div class="rt-mini-cart__body">
			<?php wc_print_notices(); ?>
			<?php
				do_action( 'woocommerce_before_mini_cart_contents' );

				get_template_part( 'woocommerce/cart/mini-cart-item');

				do_action( 'woocommerce_mini_cart_contents' );
			?>
		</div>

		<div class="rt-mini-cart__footer">

			<p class="woocommerce-mini-cart__total total"><?php _e( 'Subtotal', 'woocommerce' ); ?>: <strong> <?php echo WC()->cart->get_cart_subtotal(); ?></strong></p>

			<?php do_action( 'woocommerce_widget_shopping_cart_before_buttons' ); ?>

			<p class="woocommerce-mini-cart__buttons buttons"><?php do_action( 'woocommerce_widget_shopping_cart_buttons' ); ?></p>
		
		</div>
		
		<input type="hidden" name="cart_count_total" value="<?php echo WC()->cart->get_cart_contents_count() ?>">
		
	<?php else : ?>

		<div class="rt-mini-cart__empty">
			<i class="ti-shopping-cart"></i>
			<p class="woocommerce-mini-cart__empty-message"><?php _e( 'No products in the cart.', 'woocommerce' ); ?></p>
			<a class="rt-btn rt-btn--border" href="<?php echo  get_permalink(wc_get_page_id('shop'))?>"><?php echo __('Return to shop','woocommerce');?></a>
		</div>

	<?php endif; ?>

<?php do_action( 'woocommerce_after_mini_cart' ); ?>
</div>