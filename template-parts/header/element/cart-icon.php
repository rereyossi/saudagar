
<?php
$cart_icon = '<i class="ti-shopping-cart rt-header__cart-icon js-cart-trigger"></i>';

if(rt_option('header_cart_icon', 'cart-light') == 'shopping-basket'){
  $cart_icon = '<i class="fa fa-shopping-basket rt-header__cart-icon js-cart-trigger"></i>';
}elseif(rt_option('header_cart_icon', 'cart-light') == 'shopping-cart'){
  $cart_icon = '<i class="fa fa-shopping-cart rt-header__cart-icon js-cart-trigger"></i>';
}
?>

<?php if (rt_option('ajax_mini_cart', true)): ?>

<div  class="rt-header__element rt-header-icon rt-header__cart">
  <?php echo $cart_icon ?>
  <span class="rt-header__cart-count js-cart-total"><?php echo WC()->cart->get_cart_contents_count()?></span>
</div>

<?php else: ?>

<div class="rt-header__element rt-header-icon rt-header__cart">
  <a href="<?php echo get_permalink(wc_get_page_id('cart')); ?>">
    <?php echo $cart_icon ?>
    <span class="rt-header__cart-count js-cart-total"><?php echo WC()->cart->get_cart_contents_count()?></span>
  </a>
</div>

<?php endif ?>