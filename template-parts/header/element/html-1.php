<?php if(rt_option('header_html_1')): ?>
  <div  class="rt-header__element rt-header__html">

  <?php if (rt_option('header_html_1_shortcode')): ?>
    <?php echo do_shortcode(rt_option('header_html_1')) ?>
  <?php else: ?>

      <?php if(rt_option('header_html_1_icon')):?>
          <i class="<?php echo 'mr-5 fa fa-' . rt_option('header_html_1_icon') ?>"></i>
      <?php endif;?>

      <?php echo rt_option('header_html_1', 'Your Text/HTML') ?>
  <?php endif; ?>

  </div>
<?php endif ?>
