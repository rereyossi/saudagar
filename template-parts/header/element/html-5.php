<?php if(rt_option('header_html_5')): ?>
  <div  class="rt-header__element rt-header__html">

  <?php if (rt_option('header_html_5_shortcode')): ?>
    <?php echo do_shortcode(rt_option('header_html_5')) ?>
  <?php else: ?>

      <?php if(rt_option('header_html_5_icon')):?>
          <i class="<?php echo 'mr-5 fa fa-' . rt_option('header_html_5_icon') ?>"></i>
      <?php endif;?>

      <?php echo rt_option('header_html_5', 'Your Text/HTML') ?>
  <?php endif; ?>

  </div>
<?php endif ?>
