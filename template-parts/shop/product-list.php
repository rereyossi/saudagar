<div class="rt-cart-item">
	<div class="rt-cart-item__thumbnail rt-img rt-img-full">
		<?php
		$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image('thumbnail'), $cart_item, $cart_item_key );

		if ( ! $product_permalink ) {
			echo $thumbnail; // PHPCS: XSS ok.
		} else {
			printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
		}
		?>
	</div>
	<div class="rt-cart-item__body">
		<h5 class="rt-cart-item__title">
			<?php
			if ( ! $product_permalink ) {
				echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
			} else {
				echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
			}

			do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

			// Meta data.
			echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

			// Backorder notification.
			if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
				echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
			}
			?>
		</h5>
		<div class="rt-cart-item__meta">
			<?php
			if ( $_product->is_sold_individually() ) {
				$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
			} else {
				$product_quantity = woocommerce_quantity_input( array(
					'input_name'   => "cart[{$cart_item_key}][qty]",
					'input_value'  => $cart_item['quantity'],
					'max_value'    => $_product->get_max_purchase_quantity(),
					'min_value'    => '0',
					'product_name' => $_product->get_name(),
				), $_product, false );
			}

			echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
			?>
		</div>
		<div class="rt-cart-item__price">
			<?php
				echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
			?>
		</div>
		<div class="rt-cart-item__remove product-remove">
			<?php
				// @codingStandardsIgnoreLine
				echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
					'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
					esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
					__( 'Remove this item', 'woocommerce' ),
					esc_attr( $product_id ),
					esc_attr( $_product->get_sku() )
				), $cart_item_key );
			?>
		</div>
	</div>
</div>