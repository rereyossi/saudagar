<div class="rt-panel-cart rt-sidepanel  js-sidepanel" data-trigger=".js-cart-trigger">

    <div class="rt-sidepanel__overlay js-sidepanel-close"></div>

    <div class="rt-sidepanel__inner">

        <div class="rt-sidepanel__header">
            <a class="rt-sidepanel__close js-sidepanel-close"><i class="ti-close"></i></a>
            <h4 class="rt-sidepanel__title"><?php _e('Shopping Cart', RT_THEME_DOMAIN)?></h4>
        </div>

        <div class="rt-sidepanel__body woocommerce widget_shopping_cart">
            <div class="widget_shopping_cart_content">
                    <?php get_template_part('woocommerce/cart/mini-cart'); ?>
            </div>
        </div>

    </div>

</div>