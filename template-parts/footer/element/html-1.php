<?php 
$text_option = rt_option('footer_bottom_text', '@Copyright '.get_bloginfo('name').'. All Rights Reserved');
$text_format = str_replace('{{year}}', date("Y"), $text_option);
?>
<?php if(rt_option('footer_bottom_text', true)): ?>
<span class="rt-footer__bottom-text"><?php echo $text_format ?></span>
<?php endif ?>
