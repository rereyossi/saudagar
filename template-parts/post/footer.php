<div class="rt-post__footer">
  <a href="<?php the_permalink() ?>" class="rt-post__readmore">
    <?php echo __('Read More',RT_THEME_DOMAIN) ?>
  </a>
</div>
