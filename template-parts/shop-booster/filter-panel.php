<div id="panel-product-filter" class="wbs-filter-panel rt-sidepanel  rt-sidepanel--left js-sidepanel" data-trigger=".js-filter-panel-trigger">

    <div class="rt-sidepanel__overlay js-sidepanel-close"></div>

    <div class="rt-sidepanel__inner">


        <div class="rt-sidepanel__header">
            <a class="rt-sidepanel__close js-sidepanel-close"><i class="ti-close"></i></a>
            <h4 class="rt-sidepanel__title"><?php _e('Filter', 'webforia-shop-booster')?></h4>
        </div>

        <div class="rt-sidepanel__body">
            <?php if(is_active_sidebar('retheme_filter_sidebar')): ?>
                 <?php dynamic_sidebar('retheme_filter_sidebar'); ?>
            <?php else: ?>
                <?php
                // default filter
                $categories = array(
                    'before_widget' => '<div class="rt-widget rt-widget--aside widget_product_categories">',
                    'after_widget' => '</div>',
                    'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
                    'after_title' => '</h3></div>',
                );

                $price = array(
                    'before_widget' => '<div class="rt-widget rt-widget--aside widget_price_filter">',
                    'after_widget' => '</div>',
                    'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
                    'after_title' => '</h3></div>',
                );

                $rating = array(
                    'before_widget' => '<div class="rt-widget rt-widget--aside widget_rating_filter">',
                    'after_widget' => '</div>',
                    'before_title' => '<div class="rt-widget__header"><h3 class="rt-widget__title">',
                    'after_title' => '</h3></div>',
                );

                the_widget('WC_Widget_Product_Categories', array('title'=>'Categories'), $categories);
                the_widget('WC_Widget_Price_Filter', array('title'=>'Filter by price'), $price);
                the_widget('WC_Widget_Rating_Filter', array('title'=>'Filter by rating'), $rating);?>
            <?php endif ?>
        </div>

    </div>

</div>