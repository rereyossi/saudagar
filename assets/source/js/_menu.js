!(function($) {
  $.fn.retheme_menu = function(args) {
    return this.each(function() {
      var element = $(this);

      var defaults = {
        animatein: "transition.fadeIn",
        animateout: "transition.fadeOut",
        duration: "300",
        easing: "easeIn",
        callback: ""
      };

      var meta = element.data();
      var options = $.extend(defaults, args, meta);

      var selector = {
        item: ".rt-menu__item",
        submenu: ".rt-menu__submenu",
        has_submenu: ".has-submenu",
        menu_main: ".rt-menu__main",
        arrow: ".rt-menu__arrow",
        trigger: ".js-menu-trigger",
        close: ".js-menu-close"
      };
      /**
       * add arrow if menu find submenu
       */
      element
        .not(".rt-menu--bar-simple")
        .find(selector.item)
        .each(function() {
          if ($(this).find("ul").length > 0) {
            $(this)
              .children("a")
              .css("padding-right", "30px")
              .after(
                "<span class='rt-menu__arrow'><i class='fa fa-angle-down'></i></span>"
              );
          }
        });

      /**
       * Set Trigger
       * if want trigger from another selector
       * work in menu canvas, menu full screen
       */
      if (options.trigger) {
        var trigger = $(options.trigger);
      } else {
        var trigger = element.find(selector.trigger);
      }

      /*=================================================
      * Menu Bar Style
      /*================================================= */
      var menu_horizontal = function() {
        /*
         * show submenu if hover
         */
        element.find(selector.item).on("mouseenter", function() {
          $(this).addClass("is-active");

          $(this)
            .find(selector.submenu + ":first")
            .velocity("stop")
            .velocity(options.animatein, {
              duration: options.duration,
              easing: options.easing,
              complete: function() {
                $(this).addClass("is-active");
                $(this)
                  .children(selector.arrow)
                  .addClass("is-active");
              }
            });
        });

        /*
         * Hidden submenu if mouse leave
         */
        element.find(selector.item).on("mouseleave", function() {
          $(this).removeClass("is-active");

          $(this)
            .find(selector.submenu + ":first")
            .velocity("reverse", {
              display: "none",
              complete: function() {
                $(this).removeClass("is-active");
                $(this)
                  .children(selector.arrow)
                  .removeClass("is-active");
              }
            });
        });

        /* End menu bar*/
      };

      /*=================================================
      * Menu Dropdown Style
      /*================================================= */
      var menu_vertical = function() {
        element
          .find(selector.item)
          .on("click", ">" + selector.arrow, function(event) {
            event.preventDefault();
            

            if ($(this).hasClass("is-active")) {
              $(this).removeClass("is-active");
              $(this)
                .parent()
                .children(selector.submenu)
                .velocity('stop')
                .velocity("slideUp", {
                  duration: options.duration,
                });
            } else {
              $(this).addClass("is-active");
              $(this)
                .parent()
                .children(selector.submenu)
                .velocity("slideDown", {
                  duration: options.duration,
                });
            }
          });

        /* End menu dropdown */
      };


      /*=================================================
      * MENU INIT
      /*================================================= */
      var menuInit = function() {
        if (element.hasClass("rt-menu--horizontal")) {
          menu_horizontal();
        } else if (element.hasClass("rt-menu--vertical")) {
          menu_vertical();
        }
      };

      menuInit();

    }); /*end each*/
  }; /*end plugin*/
})(window.jQuery);


