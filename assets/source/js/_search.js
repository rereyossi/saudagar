! function ($) {
  $.fn.retheme_search = function (args) {

    return this.each(function () {


      var element = $(this);

      var defaults = {
        animatein: 'transition.fadeIn',
        animateout: 'transition.fadeOut',
        duration: '300',
        easing: 'easeIn',

      };
      var selector = {
        close: '.js-search-close',
        trigger: '.js-search-trigger',

      }

      var meta = element.data();
      var options = $.extend(defaults, args, meta);

      var active = function () {
        if (element.hasClass('rt-search--modal')) {

          element
            .velocity('transition.shrinkIn', {
            duration: options.duration,
            easing: options.easing,
            display: 'flex',
          }).addClass('is-active');
          

          element.find('.rt-search__inner')
            
            .velocity('transition.slideUpBigIn', {
              delay: 300,
              duration: options.duration,
              easing: options.easing,
              display: 'flex',
            });
            $("html").css("overflow-y", "hidden");

        } else {
           element.velocity({
             translateY: ['0%', '-100%']
           }, {
             duration: options.duration,
             easing: options.easing,
             display: 'block',
             complete: function (elements) {
               element.addClass('is-active');
             }
           });


        }
      }



      var deactive = function () {

        element.velocity('reverse', {
          display: 'none',
        }).removeClass('is-active');


        if (element.hasClass('rt-search--modal')) {
          element.find('.rt-search__inner')
            .velocity('reverse', {
              display: 'none',
            });
          $("html").css("overflow-y", "scroll");
        }

      }

      $(selector.trigger).on('click', function (event) {
        event.preventDefault();

        if (element.hasClass('is-active')) {
          deactive();

        } else {
          active();
        }
      });


      // // disable clik self element
      // element.on('click', function (event) {
      //  event.preventDefault();
      // });

      // close
      $(selector.close).on('click', function (event) {
         event.preventDefault();
        if (element.hasClass('is-active')) {
         
          deactive();
        }

      });

    }); /*end each*/
  }; /*end plugin*/

}(window.jQuery);