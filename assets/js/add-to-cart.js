jQuery(document).ready(function ($) {
  /**
   * Open sidepanel after product add to cart success
   */
  var cart_panel = $(".rt-panel-cart");
  jQuery(document.body).on("added_to_cart", function (elements) {
    if (!cart_panel.hasClass("is-active")) {
      cart_panel.retheme_sidepanel({
        action: "open",
      });
    }
  });
  /*=================================================
   *  ADD PRODUCT TO CART ON SINGLE PAGE
   * =================================================== */
  jQuery(
    ".pro .type-product:not(.product-type-external) .single_add_to_cart_button, .product_addtocart"
  ).on("click", function (event) {
    event.preventDefault();

    var product_id = jQuery(this).val();
    var variation_id = jQuery('input[name="variation_id"]').val();
    var quantity = jQuery('input[name="quantity"]').val();
    var button = $(this);

    if (button.hasClass("disabled")) {
      return false;
    }

    if (variation_id) {
      var productID = variation_id;
    } else {
      var productID = product_id;
    }

    jQuery.ajax({
      url: "?wc-ajax=add_to_cart",
      type: "POST",
      data: {
        product_id: productID,
        quantity: quantity,
      },
      beforeSend: function (response) {
        button.addClass("loading");
      },
      success: function (results) {
        if (results.error) {
          location.reload();
        }

        $(".widget_shopping_cart_content").html(
          results.fragments["div.widget_shopping_cart_content"]
        );

        $(".js-cart-total").html($("input[name='cart_count_total']").val());

        button.removeClass("loading");

        cart_panel.retheme_sidepanel({
          action: "open",
        });
      },
      error: function (results) {
        button.removeClass("loading");
      },
    });
  });

  /*=================================================;
    /* UPDATE QUANTITY MINI CART
    /*================================================= */
  var timeout;
  jQuery(document).on(
    "click",
    ".js-cart-item .minus, .js-cart-item .plus",
    function (event) {
      event.preventDefault();

      var element = $(this);
      if (timeout !== undefined) {
        clearTimeout(timeout);
      }
      timeout = setTimeout(function () {
        var cart = element.closest(".js-cart-item");
        var product_id = cart.find("[name='product_id']").val();
        var quantity = cart.find("[name='quantity']").val();
        var cart_item_key = cart.find("[name='cart_item_key']").val();
        jQuery.ajax({
          url: rt_product_object.ajaxurl,
          type: "POST",
          data: {
            action: "retheme_update_cart_result",
            product_id: product_id,
            quantity: quantity,
            cart_item_key: cart_item_key,
            check_nonce: rt_product_object.check_nonce,
          },
          beforeSend: function (response) {
            cart.addClass("rt-loader-spin");
          },
          success: function (results) {
            $(document.body).trigger("wc_fragment_refresh");
          },
        });
      }, 500);
    }
  );

  /*=================================================;
    /* DELETE CART ITEM
    /*================================================= */
  jQuery(document).on("click", ".js-remove-cart-button", function (event) {
    event.preventDefault();
    var cart = $(this).closest(".js-cart-item");
    var product_id = cart.find("[name='product_id']").val();
    var cart_item_key = cart.find("[name='cart_item_key']").val();

    jQuery.ajax({
      url: "?wc-ajax=remove_from_cart",
      type: "POST",
      data: {
        cart_item_key: cart_item_key,
      },
      beforeSend: function (response) {
        cart.addClass("rt-loader-spin");
      },
      success: function (results) {
        $(".widget_shopping_cart_content").html(
          results.fragments["div.widget_shopping_cart_content"]
        );

        $(".js-cart-total").html($("input[name='cart_count_total']").val());
      },
      error: function (results) {
        button.removeClass("loading");
      },
    });
  });

  /*=================================================;
  /* UPDATE ON CART PAGE
  /*================================================= */
  $(".woocommerce-cart").on("click", ".quantity_change span", function () {
    var update_cart = $(".update_cart");
    setTimeout(() => {
      $(".woocommerce-message").remove();
      update_cart.prop("disabled", false);
      update_cart.trigger("click");
    }, 500);
  });
});
