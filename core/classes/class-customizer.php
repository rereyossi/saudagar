<?php
namespace Retheme;

use Retheme\Helper;

/**
 * @author Reret Yosyanto hello@rereyossi.com
 * @required - Class Hellper, Kirki Library
 * @desc - function library for kirki customizer
 */
class Customizer_Base
{

    public $breakpoint_large = '@media (min-width: 960px)';
    public $breakpoint_medium = '@media (max-width: 768px)';
    public $breakpoint_small = '@media (max-width: 468px)';

    public function __construct()
    {
        $this->add_config();
    }
    

    /**
     * Add customizer options
     */
    public function add_config()
    {
        \Kirki::add_config('retheme_customizer', array(
            'capability' => 'edit_theme_options',
            'option_type' => 'theme_mod',
        ));
    }

    /**
     * Add panel
     * required kirki
     */
    public function add_panel($name, $args)
    {
        \Kirki::add_panel($name, $args);
    }

    /**
     * Add section
     * required kirki
     */
    public function add_section($panel = '', $sections)
    {
        if (rt_is_free()) {
            \Kirki::add_section(
                'theme_pro',
                [
                    'title' => esc_html__('More Options', RT_THEME_DOMAIN),
                    'type' => 'link',
                    'button_text' => 'Get ' . rt_var('product-name') . ' Pro',
                    'button_url' => rt_var('product-url'),
                    'priority' => 1,
                ]
            );

        }

        foreach ($sections as $section_id => $section) {
            if (!empty($section[0])) {
                $section_args['title'] = $section[0];
            }

            if (!empty($section[1])) {
                $section_args['description'] = $section[1];
            }

            if (!empty($panel)) {
                $section_args['panel'] = $panel;
            }

            if (!empty($section[2])) {
                $section_args['type'] = $section[2];
            }

            \Kirki::add_section($section_id . '_section', $section_args);

        }
    }

    /**
     * Add field control
     */
    public function add_field($args)
    {
        \Kirki::add_field('retheme_customizer', $args);
    }

    /**
     * Add header control
     */
    public function add_header($args = array())
    {
        $child = '';

        $tooltip = !empty($args['tooltip']) ? $args['tooltip'] : '';

        if (!empty($args['child'])) {
            foreach ($args['child'] as $key => $value) {
                $data[] = '#customize-control-' . $value;
            }
            $child = join(',', $data);
        }

        $this->add_field(array(
            'type' => 'custom',
            'settings' => 'retheme_header_' . $args['settings'],
            'section' => $args['section'],
            'class' => !empty($args['class']) ? $args['class'] : '',
            'default' => '<div class="retheme-header" data-child="' . $child . '">' . $args['label'] . '</div>',
            'active_callback' => (!empty($args['active_callback'])) ? $args['active_callback'] : '',
            'tooltip' => $tooltip,
        ));
    }

    public function add_sub_header($args = array())
    {
        $this->add_field(array(
            'type' => 'custom',
            'settings' => 'retheme_sub_header_' . $args['settings'],
            'section' => $args['section'],
            'class' => !empty($args['class']) ? $args['class'] : '',
            'default' => '<div class="retheme_subheader">' . $args['label'] . '</div>',
        ));
    }

    /**
     * Responsive control with mobile choose
     * @desc - Add tablet and mobile control
     * @param $args - argument from control
     * @return HTML - control field
     */
    public function add_field_responsive($args = array())
    {
        $output_tablet = array();
        $output_mobile = array();

        /** Add media query each element output */
        if (!empty($args['output'])) {

            foreach ($args['output'] as $key => $value) {
                $output_tablet[] = wp_parse_args(['media_query' => $this->breakpoint_medium], $value);
                $output_mobile[] = wp_parse_args(['media_query' => $this->breakpoint_small], $value);
            }

        } elseif (!empty($args['element'])) {
            $output_tablet[] = array('media_query' => $this->breakpoint_medium, 'element' => $args['element']);
            $output_mobile[] = array('media_query' => $this->breakpoint_small, 'element' => $args['element']);
        }

        /** Show responsive control */
        $this->add_field(wp_parse_args(array(
            'device' => 'desktop',
        ), $args));

        $this->add_field(wp_parse_args(array(
            'settings' => $args['settings'] . '_tablet',
            'device' => 'tablet',
            'output' => $output_tablet,
        ), $args));

        $this->add_field(wp_parse_args(array(
            'settings' => $args['settings'] . '_mobile',
            'device' => 'mobile',
            'output' => $output_mobile,
        ), $args));

    }

    /**
     * Background Group Control
     *
     * @param array $args
     * @return void
     */

    public function add_field_background($args = array())
    {
        $element = !empty($args['element']) ? $args['element'] : '';

        if (!empty($args['pseudo'])) {
            $this->add_field(wp_parse_args($args, array(
                'label' => __('Background Color', RT_THEME_DOMAIN),
                'settings' => $args['settings'],
                'type' => 'multicolor',
                'choices' => [
                    'normal' => __('Normal', RT_THEME_DOMAIN),
                    'hover' => __('Hover', RT_THEME_DOMAIN),

                ],
                'default' => [
                    'normal' => '',
                    'hover' => '',
                ],
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'background-color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => $this->selector($element),
                        'property' => 'background-color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'hover',
                    ),

                ),
                'transport' => 'auto',
            )));
        } else {
            $this->add_field(wp_parse_args($args, array(
                'label' => __('Background Color', RT_THEME_DOMAIN),
                'type' => 'color',
                'choices' => array(
                    'alpha' => true,
                ),
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'background-color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                    ),
                ),
                'transport' => 'auto',
            )));

        }

    }
    /**
     * Color Group Control
     *
     * @param array $args
     * @return void
     */
    public function add_field_color($args = array())
    {
        $element = !empty($args['element']) ? $args['element'] : '';

        if (!empty($args['pseudo'])) {
            $this->add_field(wp_parse_args($args, array(
                'label' => __('Color', RT_THEME_DOMAIN),
                'settings' => $args['settings'],
                'type' => 'multicolor',
                'choices' => [
                    'normal' => __('Normal', RT_THEME_DOMAIN),
                    'hover' => __('Hover', RT_THEME_DOMAIN),

                ],
                'default' => [
                    'normal' => '',
                    'hover' => '',
                ],
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => $this->selector($element, $args['pseudo']),
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'property' => 'color',
                        'choice' => 'hover',
                    ),

                ),
                'transport' => 'auto',
            )));
        } else {
            $this->add_field(wp_parse_args($args, array(
                'label' => __('Color', RT_THEME_DOMAIN),
                'type' => 'color',
                'choices' => array(
                    'alpha' => true,
                ),
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                    ),
                ),
                'transport' => 'auto',
            )));

        }

    }

    /**
     * Link Group Control
     *
     * @param array $args
     * @return void
     */
    public function add_field_link($args = array())
    {
        $element = !empty($args['element']) ? $args['element'] : '';

        if (!empty($args['pseudo'])) {
            $this->add_field(wp_parse_args($args, array(
                'label' => __('Link', RT_THEME_DOMAIN),
                'settings' => $args['settings'],
                'type' => 'multicolor',
                'choices' => [
                    'normal' => __('Normal', RT_THEME_DOMAIN),
                    'hover' => __('Hover', RT_THEME_DOMAIN),

                ],
                'default' => [
                    'normal' => '',
                    'hover' => '',
                ],
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => $this->selector($element, $args['pseudo']),
                        'property' => 'color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'hover',
                    ),

                ),
                'transport' => 'auto',
            )));
        } else {
            $this->add_field(wp_parse_args($args, array(
                'label' => __('Link', RT_THEME_DOMAIN),
                'type' => 'color',
                'choices' => array(
                    'alpha' => true,
                ),
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                    ),
                ),
                'transport' => 'auto',
            )));

        }
    }

    /**
     * Border Color Control
     *
     * @param array $args
     * @return void
     */
    public function add_field_border_color($args = array())
    {
        $element = !empty($args['element']) ? $args['element'] : '';

        if (!empty($args['pseudo'])) {
            $this->add_field(wp_parse_args($args, array(
                'label' => __('Border Color', RT_THEME_DOMAIN),
                'settings' => $args['settings'],
                'type' => 'multicolor',
                'choices' => [
                    'normal' => __('Normal', RT_THEME_DOMAIN),
                    'hover' => __('Hover', RT_THEME_DOMAIN),

                ],
                'default' => [
                    'normal' => '',
                    'hover' => '',
                ],
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'border-color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'normal',
                    ),
                    array(
                        'element' => $this->selector($element, $args['pseudo']),
                        'property' => 'border-color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                        'choice' => 'hover',
                    ),

                ),
                'transport' => 'auto',
            )));
        } else {
            $this->add_field(wp_parse_args($args, array(
                'label' => __('Border Color', RT_THEME_DOMAIN),
                'type' => 'color',
                'choices' => array(
                    'alpha' => true,
                ),
                'output' => array(
                    array(
                        'element' => $element,
                        'property' => 'border-color',
                        'suffix' => !empty($args['suffix']) ? $args['suffix'] : '',
                    ),
                ),
                'transport' => 'auto',
            )));

        }
    }

    /**
     * Border Radius Control
     *
     * @param array $args
     * @return void
     */
    public function add_field_border_radius($args = array())
    {
        $label = !empty($args['label']) ? $args['label'] : 'Border Radius';
        $element = !empty($args['element']) ? $args['element'] : '';

        /**
         * Merge default array with array from control
         * @param array $args, $default
         */
        $this->add_field(wp_parse_args($args, array(
            'label' => __($label, RT_THEME_DOMAIN),
            'type' => 'slider',
            'choices' => array(
                'min' => '0',
                'max' => '100',
                'step' => '1',
            ),
            'output' => array(
                array(
                    'element' => $element,
                    'property' => 'border-radius',
                    'units' => 'px',
                ),
            ),
            'transport' => 'auto',
        )));
    }

    /**
     * Animation Control
     *
     * @param array $args
     * @return void
     */
    public function add_field_animation($args = array())
    {
        $element = !empty($args['element']) ? $args['element'] : '';
        /**
         * Merge default array with array from control
         * @param array $args, $default
         */
        $this->add_field(wp_parse_args($args, array(
            'type' => 'select',
            'label' => __('Animation', 'admin_domain'),
            'settings' => $args['settings'],
            'default' => 'transition.fadeIn',
            'choices' => Helper::get_animation_in(),
        )));

        $this->add_field(array(
            'type' => 'number',
            'label' => __('Duration', RT_THEME_DOMAIN),
            'settings' => $args['settings'] . '_duration',
            'default' => 300,
            'choices' => array(
                'min' => 120,
                'max' => 1000,
            ),
        ));

    }

    /*
     * Button group
     */
    public function add_field_button($args = array())
    {
        
        $class = !empty($args['class']) ? $args['class'] : '';
        $element = !empty($args['element']) ? $args['element'] : '';

        $this->add_field(array(
            'type' => 'typography',
            'settings' => $args['settings'] . '_typography',
            'label' => __('Typography', 'kirki'),
            'section' => $args['section'],
            'default' => [
                'font-family' => '',
                'variant' => '',
                'font-size' => '',
                'letter-spacing' => '0',
                'text-transform' => '',
            ],
            'priority' => 10,
            'transport' => 'auto',
            'output' => [
                [
                    'element' => $element,
                ],
            ],
        ));

        $this->add_field_color(array(
            'settings' => $args['settings'] . '_color',
            'section' => $args['section'],
            'class' => $class,
            'element' => $element,
            'pseudo' => 'all',
        ));

        $this->add_field_background(array(
            'settings' => $args['settings'] . '_background',
            'section' => $args['section'],
            'class' => $class,
            'element' => $element,
            'pseudo' => 'all',
        ));

        $this->add_field_border_color(array(
            'settings' => $args['settings'] . '_border_color',
            'section' => $args['section'],
            'class' => $class,
            'element' => $element,
            'pseudo' => 'all',
        ));

        $this->add_field_border_radius(array(
            'settings' => $args['settings'] . '_border_radius',
            'section' => $args['section'],
            'class' => $class,
            'element' => $element,
            'pseudo' => 'all',
        ));

        $this->add_field_padding(array(
            'settings' => $args['settings'],
            'section' => $args['section'],
            'element' => $element,
        ));

    }

    public function add_field_padding($args = array())
    {

        $this->add_field_responsive(wp_parse_args($args, array(
            'type' => 'dimensions',
            'label' => __('Padding', RT_THEME_DOMAIN),
            'settings' => $args['settings'],
            'section' => $args['section'],
            'description' => 'Use css unit px, %',
            'default' => array(
                'left' => '',
                'top' => '',
                'right' => '',
                'bottom' => '',
            ),
            'output' => array(
                array(
                    'element' => $args['element'],
                    'property' => 'padding',
                ),

            ),
            'transport' => 'auto',
        )));
    }

     public function add_field_margin($args = array())
    {

        $this->add_field_responsive(wp_parse_args($args, array(
            'type' => 'dimensions',
            'label' => __('Margin', RT_THEME_DOMAIN),
            'settings' => $args['settings'],
            'section' => $args['section'],
            'description' => 'Use css unit px, %',
            'default' => array(
                'left' => '',
                'top' => '',
                'right' => '',
                'bottom' => '',
            ),
            'output' => array(
                array(
                    'element' => $args['element'],
                    'property' => 'margin',
                ),

            ),
            'transport' => 'auto',
        )));
    }
    /*
     * Form group
     */
    public function add_field_form($args = array())
    {

        $class = !empty($args['class']) ? $args['class'] : '';
        $element = !empty($args['element']) ? $args['element'] : '';

        $this->add_field_color(array(
            'label' => 'Placeholder Color',
            'settings' => $args['settings'] . '_color',
            'section' => $args['section'],
            'class' => $class,
            'element' => $element,
            'pseudo' => 'hover',
        ));

        $this->add_field_background(array(
            'settings' => $args['settings'] . '_background',
            'section' => $args['section'],
            'class' => $class,
            'element' => $element,
            'pseudo' => 'hover',
        ));

        $this->add_field_border_color(array(
            'settings' => $args['settings'] . '_border_color',
            'section' => $args['section'],
            'class' => $class,
            'element' => $element,
            'pseudo' => 'hover',
        ));
        $this->add_field_border_radius(array(
            'settings' => $args['settings'] . '_border_radius',
            'section' => $args['section'],
            'class' => $class,
            'element' => $element,
        ));
    }

    /**
     * Desc
     */
    public function add_desc($args)
    {
        $this->add_field(array(
            'type' => 'custom',
            'settings' => $args['settings'],
            'section' => $args['section'],
            'default' => '<div class="retheme-desc">' . $args['label'] . '</div>',
        ));
    }

    /*
     * Merge selector for hover
     */
    public function selector($selector, $pseudo = '')
    {
        $data = explode(",", $selector);
        $element = array();

        foreach ($data as $key => $value) {

            $element[] = $value . ':hover';
            $element[] = $value . ':active';
            $element[] = $value . ':focus';

        }
        return implode(', ', $element);

    }

    // end class
}

new Customizer_Base;
