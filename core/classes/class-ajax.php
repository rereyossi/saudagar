<?php
namespace Retheme;

use Retheme\Helper;

class Class_Ajax
{
    public function __construct()
    {
        add_action('wp_ajax_ajax_loop_result', [$this, 'loop_result']);
        add_action('wp_ajax_nopriv_ajax_loop_result', [$this, 'loop_result']);

        add_action('wp_enqueue_scripts', [$this, 'load_scripts']);

    }

    /**
     * return query
     *
     * @return [post]
     */
    public function loop_result()
    {

        if (check_ajax_referer('retheme-loop-nonce', 'check_nonce')) {
            // get setting from json
            $settings = $_POST['query'];

            // Merge query from setting json with next/prev page
            // show only publish post
            $args = wp_parse_args(Helper::query($settings), array(
                'paged' => $_POST['page'],
                'post_status' => 'publish',
            ));

            $the_query = new \WP_Query($args);

            ob_start();

            if ($the_query->have_posts()) {
                while ($the_query->have_posts()): $the_query->the_post();

                    include locate_template($settings['template_part'] . '.php');

                endwhile;

                wp_reset_postdata();

            } else {
                rt_post_none();
            }

            $ajax_results = ob_get_clean();

            wp_send_json($ajax_results);
        }

    }

    /**
     * load scripts
     *
     * @return [inject ajax-loop.js, js variable]
     */
    public function load_scripts()
    {
        wp_enqueue_script('ajax_loop', get_template_directory_uri() . '/core/classes/assets/js/ajax-loop.min.js', array('jquery'), '1.2.0', true);

        wp_localize_script(
            'ajax_loop',
            'ajax_loop',
            array(
                'ajaxurl' => admin_url('admin-ajax.php'),
                'check_nonce' => wp_create_nonce('retheme-loop-nonce'),
            )
        );
    }

    /* end class */
}

new Class_Ajax;
