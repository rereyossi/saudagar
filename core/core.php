<?php
include_once dirname(__FILE__) . '/vendor/vendor.php';
include_once dirname(__FILE__) . '/admin/admin.php';
include_once dirname(__FILE__) . '/classes/class.php';

if(empty($_GET['wc-ajax'])){
    include_once dirname(__FILE__) . '/customizer/customizer.php';
}
include_once dirname(__FILE__) . '/includes/include.php';
include_once dirname(__FILE__) . '/elementor/elementor.php';

if (rt_is_woocommerce()) {
    include_once dirname(__FILE__) . '/shop-booster/shop-booster.php';
    include_once dirname(__FILE__) . '/woocommerce/woocommerce.php';
}
