jQuery(document).ready(function ($) {
    $(".retheme-header").parents('li')
        .addClass('control-header')
        .attr('data-control', 'control-header');

    $(".control-header").append('<div class="control-arrow"><i class="fa fa-plus" aria-hidden="true"></i><i class="fa fa-minus" aria-hidden="true"></i></div>');


    var element = $("li.customize-control");

    element.each(function () {

        if ($(this).attr('id')) {

            var id = $(this).attr('id');
            var control_id = id.replace('customize-control-', '');
            var control_class = '';

            /*=================================================
            * Responsive Control
            /*================================================= */

            if (_wpCustomizeSettings['controls'][control_id] && _wpCustomizeSettings['controls'][control_id]['device']) {
                var control_device = _wpCustomizeSettings['controls'][control_id]['device'];
                var device_icon = ('<ul class="responsive-switchers"><li class="desktop"><span class="preview-desktop" data-device="desktop"><i class="dashicons dashicons-desktop"></i></span></li><li class="tablet"><span class="preview-tablet" data-device="tablet"><i class="dashicons dashicons-tablet"></i></span></li><li class="mobile"><span class="preview-mobile" data-device="mobile"><i class="dashicons dashicons-smartphone"></i></span></li></ul>');

                if (control_device.length != 0) {
                    $(this).addClass('control-responsive control-device-' + control_device)
                        .attr('data-responsive', control_device);
                    /**
                     * insert device icon each responsive control
                     */
                    wp.customize.control(control_id, function (control) {
                        control.deferred.embedded.done(function () {
                            control.container.find('label').append(device_icon);
                        });
                    });

                }




            }

            /*=================================================
            * Class Control
            /*================================================= */
            if (_wpCustomizeSettings['controls'][control_id] && _wpCustomizeSettings['controls'][control_id]['class']) {

                var control_class = _wpCustomizeSettings['controls'][control_id]['class'];

                /**
                 * set Class list control
                 */
                $(this).addClass(control_class)
                  .attr('data-class', control_class);

                /**
                 * add class required if find control with hidden style
                 */
                if (_wpCustomizeSettings['controls'][control_id]['required']['length']) {

                  $(this).addClass("control-required");

                }


                /**
                 * Set Class collapse
                 */
                if ($(this).hasClass(control_class)) {
                  $(this).not('.control-header')
                    .addClass('control-collapse')
                    .attr('data-control', 'control-collapse');
                }
                if ($(this).hasClass('control-collapse')) {
                  $(this).not('.control-required').show();
                }

                if ($(this).css("display") == "list-item") {
                  $(this).addClass("control-required-open");
                }

                /** Hide all control collapse */
                if($(this).hasClass('control-collapse')){
                  $(this).hide();
                }



            }

            /**
             * HIDE/SHOW PANEL
             */


            if ($(this).hasClass('control-header')) {
              $(this).on('click', function (event) {
                event.preventDefault();

                if ($(this).hasClass(control_class) && !$(this).hasClass('is-active')) {

                  // show all control class
                  $('.control-collapse').slideUp();
                  element.removeClass('is-active');

                  $(this).addClass('is-active');

                  if ($('.control-collapse.' + control_class).hasClass("control-required-open")){
                    $('.control-collapse.' + control_class).slideDown();
                  }else{
                    $(".control-collapse." + control_class).removeClass("control-required-open");
                    $('.control-collapse.' + control_class).not('.control-required').slideDown();
                  }

                } else {

                  // hide all
                  $(this).removeClass('is-active');
                  $('.control-collapse.' + control_class).slideUp();
                }
              });
            }

            // end main if
        }
        // each
    });

    // end jquery ready
});
