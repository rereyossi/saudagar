<?php
if (!class_exists('Kirki')) {
    require_once dirname(__FILE__) . '/kirki/kirki.php';
}
if (!class_exists('Breadcrumb_Trail')) {
    require_once dirname(__FILE__) . '/breadcrumb-tail.php';
}
require_once dirname(__FILE__) . '/class-tgm-plugin-activation.php';
require_once dirname(__FILE__) . '/class-gamajo-template-loader.php';
require_once dirname(__FILE__) . '/plugin-update-checker/plugin-update-checker.php';

if (!class_exists('Mobile_Detect')) {
    require_once dirname(__FILE__) . '/Mobile_Detect.php';
}

// merlin
if (!class_exists('Merlin')) {
    require_once get_parent_theme_file_path('/core/vendor/merlin/vendor/autoload.php');
    require_once get_parent_theme_file_path('/core/vendor/merlin/class-merlin.php');
    require_once get_parent_theme_file_path('/core/vendor/merlin-config.php');
}

$theme_update = Puc_v4_Factory::buildUpdateChecker('https://gitlab.com/rereyossi/saudagar/', get_template_directory(), 'saudagar');
$theme_update->setBranch('stable_release');
