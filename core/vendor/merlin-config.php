<?php
if (!class_exists('Merlin')) {
    return;
}
/**
 * Set directory locations, text strings, and settings.
 */
class Retheme_Merlin extends Merlin
{

    public $secret_key = '5c0a2dd7949698.97710041s';
    public $server_host = 'https://webforia.id/';
    /**
     * Activate the EDD license.
     *
     * This code was taken from the EDD licensing addon theme example code
     * (`activate_license` method of the `EDD_Theme_Updater_Admin` class).
     *
     * @param string $license The license key.
     *
     * @return array
     */
     public function hostserver(){
        return 'https://webforia.id/?time='.time();
    }
    protected function edd_activate_license($key)
    {
        $success = false;

        // Strings passed in from the config file.
        $strings = $this->strings;

        // Theme Name.
        $theme = ucfirst($this->theme);

        // Remove "Child" from the current theme name, if it's installed.
        $theme = str_replace(' Child', '', $theme);

        // Text strings.
        $success_message = $strings['license-json-success%s'];

        $api_params = array(
            'slm_action' => 'slm_activate',
            'secret_key' => $this->secret_key,
            'license_key' => $key,
            'registered_domain' => str_replace(array('www.', 'http://', 'https://'), '', site_url()),
        );

        // Send query to the license manager server
        $query = esc_url_raw(add_query_arg($api_params, $this->hostserver()));
        $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));

       


        // Check for error in the response
        if (is_wp_error($response)) {

            $message = esc_html__('Proses aktivasi bermasalah, pastikan Anda terhubung dengan internet', 'merlin-wp');

            return false;
        } else {
            
            $class_activation = new \Retheme\Activation;

            // License data.
            $license_data = json_decode(wp_remote_retrieve_body($response));

  

            if ($license_data->result == 'success') { //Success was returned for the license activation

                // check product refrensi

                if ($class_activation->get_product($key) == rt_var('product-slug')) {
                    update_option(rt_var('product-slug', '_status'), 'active');
                    update_option(rt_var('product-slug', '_key'), $key);

                    // extend plugin include active
                    foreach (rt_var('extend') as $key => $plugin) {
                        update_option("{$plugin}_status", 'active');
                        update_option("{$plugin}_key", $key);
                    }

                    $message = sprintf(esc_html($success_message), $theme);
                    $success = true;

                } else {
                    $class_activation->deactivation($key);
                    $message = esc_html__('Product not register for this license key', 'merlin-wp');

                }

            } else {
                //Show error to the user. Probably entered incorrect license key.

                update_option(rt_var('product-slug', '_status'), 'deactive');
                update_option(rt_var('product-slug', '_key'), '');

                // extend plugin include deactive
                foreach (rt_var('extend') as $key => $plugin) {
                    update_option("{$plugin}_status", 'deactive');
                    update_option("{$plugin}_key", '');
                }

                $message = sprintf(esc_html($license_data->message), $theme);

            }

        }

        return compact('success', 'message');

    }


// end class
}

// check license
function merlin_check_license(){
    if(rt_is_premium()){
        return true;
    }
}
add_filter('merlin_is_theme_registered', 'merlin_check_license');


// set config theme wizard
New Retheme_Merlin(
    $config = array(
        'directory' => 'core/vendor/merlin', // Location / directory where Merlin WP is placed in your theme.
        'merlin_url' => 'merlin', // The wp-admin page slug where Merlin WP loads.
        'parent_slug' => 'themes.php', // The wp-admin parent page slug for the admin menu item.
        'capability' => 'manage_options', // The capability required for this menu to be displayed to the user.
        'child_action_btn_url' => 'https://codex.wordpress.org/child_themes', // URL for the 'child-action-link'.
        'dev_mode' => true, // Enable development mode for testing.
        'license_step' => true, // EDD license activation step.
        'license_required' => false, // Require the license activation step.
        'license_help_url' => '', // URL for the 'license-tooltip'.
        'edd_remote_api_url' => '', // EDD_Theme_Updater_Admin remote_api_url.
        'edd_item_name' => RT_THEME_NAME, // EDD_Theme_Updater_Admin item_name.
        'edd_theme_slug' => RT_THEME_SLUG, // EDD_Theme_Updater_Admin item_slug.
        'ready_big_button_url' => site_url(), // Link for the big button on the ready step.
    ),

    $strings = array(
        'admin-menu' => esc_html__('Theme Setup', '@@textdomain'),
        /* translators: 1: Title Tag 2: Theme Name 3: Closing Title Tag */
        'title%s%s%s%s' => esc_html__('%1$s%2$s Themes &lsaquo; Theme Setup: %3$s%4$s', '@@textdomain'),
        'return-to-dashboard' => esc_html__('Kembali kehalaman admin', '@@textdomain'),
        'ignore' => esc_html__('Matikan fitur wizard', '@@textdomain'),
        'btn-skip' => esc_html__('Lewati', '@@textdomain'),
        'btn-next' => esc_html__('Selanjutnya', '@@textdomain'),
        'btn-start' => esc_html__('Mulai', '@@textdomain'),
        'btn-no' => esc_html__('Batal', '@@textdomain'),
        'btn-plugins-install' => esc_html__('Instal', '@@textdomain'),
        'btn-child-install' => esc_html__('Instal', '@@textdomain'),
        'btn-content-install' => esc_html__('Instal', '@@textdomain'),
        'btn-import' => esc_html__('Impor', '@@textdomain'),
        'btn-license-activate' => esc_html__('Aktifkan', '@@textdomain'),
        'btn-license-skip' => esc_html__('Nanti', '@@textdomain'),
        /* translators: Theme Name */
        'license-header%s' => esc_html__('Aktifkan %s', '@@textdomain'),
        /* translators: Theme Name */
        'license-header-success%s' => esc_html__('%s sudah diaktifkan', '@@textdomain'),
        /* translators: Theme Name */
        'license%s' => esc_html__('Masukan kunci lisensi untuk mendapatkan semua fitur premium, update, dan dukungan dari Webforia Studio', '@@textdomain'),
        'license-label' => esc_html__('Kunci lisensi', '@@textdomain'),
        'license-success%s' => esc_html__('Tema ini sudah terdaftar, silakan lanjutkan ke langkah berikutnya', '@@textdomain'),
        'license-json-success%s' => esc_html__('Tema Anda sudah di aktifkan! Premium fitur, update, dan dukungan bisa digunakan', '@@textdomain'),
        'license-tooltip' => esc_html__('Butuh bantuan?', '@@textdomain'),
        /* translators: Theme Name */
        'welcome-header%s' => esc_html__('Selamat datang di %s', '@@textdomain'),
        'welcome-header-success%s' => esc_html__('Hi. Selamat datang kembali', '@@textdomain'),
        'welcome%s' => esc_html__('Fitur Wizard ini akan membantu Anda untuk melakukan pengaturan tema, memasang plugin, dan mengimpor konten. Langkah ini hanya opsional tapi sangat Saya rekomendasikan bagi Anda yang baru di dunia WordPress.', '@@textdomain'),
        'welcome-success%s' => esc_html__('Anda mungkin sudah menjalankan fitur Wizard ini sebelumnnya, Jika Anda ingin melanjutkannya silakan klik tombol "Mulai" di bawah ini', '@@textdomain'),
        'child-header' => esc_html__('Install tema anak', '@@textdomain'),
        'child-header-success' => esc_html__('You\'re good to go!', '@@textdomain'),
        'child' => esc_html__('Aktifkan tema anak sehingga Anda dapat dengan mudah melakukan perubahan tanpa merusak tema utama.', '@@textdomain'),
        'child-success%s' => esc_html__('Tema anak Anda berhasil di install dan diaktifkan', '@@textdomain'),
        'child-action-link' => esc_html__('Pelajarin lebih lanjut tentang tema anak', '@@textdomain'),
        'child-json-success%s' => esc_html__('Tema anak Anda sudah diinstall dan diaktifkan', '@@textdomain'),
        'child-json-already%s' => esc_html__('Tema anak Anda sudah berhasil dibuat dan sekarang sudah diaktifkan', '@@textdomain'),
        'plugins-header' => esc_html__('Install Plugins', '@@textdomain'),
        'plugins-header-success' => esc_html__('Mengagumkan', '@@textdomain'),
        'plugins' => esc_html__('Mari kita instal beberapa plugin WordPress yang penting untuk membangun situs Anda.', '@@textdomain'),
        'plugins-success%s' => esc_html__('Semua plugin WordPress yang diperlukan sudah terpasang dan diperbarui. Tekan "Selanjutnya" untuk melanjutkan kepengaturan.', '@@textdomain'),
        'plugins-action-link' => esc_html__('Tingkat lanjut', '@@textdomain'),
        'import-header' => esc_html__('Impor Konten', '@@textdomain'),
        'import' => esc_html__('Impor konten tema sesuai dengan website demo', '@@textdomain'),
        'import-action-link' => esc_html__('Tingkat lanjut', '@@textdomain'),
        'ready-header' => esc_html__('Proses installasi selesai. Selamat bersenang-senang!', '@@textdomain'),
        /* translators: Theme Author */
        'ready%s' => esc_html__('Tema Anda telah diatur. Nikmati tema baru Anda yang dikembangkan oleh %s.', '@@textdomain'),
        'ready-action-link' => esc_html__('Extra', '@@textdomain'),
        'ready-big-button' => esc_html__('Kunjungi website', '@@textdomain'),
        // 'ready-link-1' => sprintf('<a href="%1$s" target="_blank">%2$s</a>', admin_url( 'index.php?page=wc-setup'), esc_html__('Setup Toko', '@@textdomain')),
        'ready-link-2' => sprintf('<a href="%1$s" target="_blank">%2$s</a>', admin_url('customize.php'), esc_html__('Mulai melakukan kostumisasi', '@@textdomain')),
        'ready-link-3' => sprintf('<a href="%1$s" target="_blank">%2$s</a>', 'https://webforia.id/kontak/', esc_html__('Hubungi dukungan tema', '@@textdomain')),

    )
);
