<?php
namespace ShopBooster;

use Retheme\Customizer_Base;

class WSB_Sticky_Product
{
    public function __construct()
    {

        $this->customizer();
        add_action('rt_footer', [$this, 'render']);
        add_filter('header_sticky', [$this, 'remove_sticky_header']);
        add_action('woocommerce_single_product_summary', [$this, 'add_product_sticky_trigger'], 25);

    }

    public function customizer()
    {
        $section = 'woocommerce_sticky_product_section';

        $customizer = new Customizer_Base;

        $customizer->add_section('shop_booster', array(
            'woocommerce_sticky_product' => array(__('Sticky Product', 'webforia-shop-booster')),
        ));

        $customizer->add_field(array(
            'type' => 'toggle',
            'settings' => 'product_sticky',
            'label' => __('Sticky Product', 'webforia-shop-booster'),
            'section' => $section,
            'default' => true,
        ));

    }

    /**
     * Add trigger for show sticky button
     *
     * @param [type] $classes
     * @return html
     */
    public function add_product_sticky_trigger($classes)
    {
        echo "<span class='js-sticky-product-trigger'></span>";
    }

    public function remove_sticky_header($args)
    {
        if (rt_option('product_sticky', true) && rt_is_woocommerce('product')) {
            $args = false;
        }
        return $args;
    }

    public function render()
    {
        if (rt_option('product_sticky', true) && rt_is_woocommerce('product')) {
            rt_get_template_part('shop-booster/product-sticky');
        }
    }

}

new WSB_Sticky_Product;
