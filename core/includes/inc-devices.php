<?php
/*=================================================
 * MOBILE - MOBILE AND TABLET
=================================================== */
// mobile and tablet
function rt_is_mobile_shortcode($atts, $content = null)
{   
     $detect = new Mobile_Detect;

    if ($detect->isMobile()) {
        return do_shortcode($content);
    } else {
        return null;
    }
}
add_shortcode('is_mobile', 'rt_is_mobile_shortcode');


/*=================================================
 * MOBILE - ONLY MOBILE
=================================================== */
function rt_is_only_mobile_shortcode($atts, $content = null)
{
    $detect = new Mobile_Detect;

    if ($detect->isMobile() && !$detect->isTablet()) {
        return do_shortcode($content);
    } else {
        return null;
    }
}
add_shortcode('is_mobile_only', 'rt_is_only_mobile_shortcode');


/*=================================================
 * MOBILE - ONLY TABLET
=================================================== */
function rt_is_only_tablet_shortcode($atts, $content = null)
{
    $detect = new Mobile_Detect;

    if ($detect->isTablet()) {
        return do_shortcode($content);
    } else {
        return null;
    }
}
add_shortcode('is_tablet_only', 'rt_is_only_tablet_shortcode');

/*=================================================
 * MOBILE - ONLY DESKTOP
=================================================== */
function rt_is_desktop_shortcode($atts, $content = null)
{
    $detect = new Mobile_Detect;

    if (!$detect->isMobile()) {
        return do_shortcode($content);
    } else {
        return null;
    }
}
add_shortcode('is_desktop', 'rt_is_desktop_shortcode');


